/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var refF= new Firebase('http://shining-heat-6268.firebaseio.com');
var devRef = new Firebase('http://shining-heat-6268.firebaseio.com/devices');

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        window.open = cordova.InAppBrowser.open;
        var div = document.getElementById('main');
        
        function hasClass(el, clss) {
            return el.className && new RegExp("(^|\\s)" +
                   clss + "(\\s|$)").test(el.className);
        }

        if(hasClass(div, "overview")) {
            devRef.on("value", function(dev) {
                var i = dev.val(); //device list
                var addToList = document.getElementById('list');
                 for (x in i) {
                    addToList.innerHTML = addToList.innerHTML +   "<li><a href='" +i[x].url +"' class='link loadURL' target='_self'>"+x +"</a></li>";
                 }
            });

            document.addEventListener('click', function(e) {
                e = e || window.event;
                var target = e.target || e.srcElement,
                    url = target.getAttribute("href");  
                 console.log('here', target, url); 
                 var ref = cordova.InAppBrowser.open(url,'_self', 'location=yes');
            }, false);
        };
    },
    // Update DOM on a Received Event
    /*receivedEvent: function(id) {
        /*var parentElement = document.getElementById(id);
        //var listeningElement = parentElement.querySelector('.listening');
        //var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }*/
};

app.initialize();

$(document).ready(function() {

    // on-click
   // overlay pw-reset
    $(".show_overlay").click(function(e) {
        e.preventDefault();
        if($("div").hasClass('overlay')){
          $("div.overlay").addClass('show');
        }
    });

    $( "#btn-login").click(function() {
      var email = $( "#top-input-loginmail").val();
      var pw = $( "#rounded-down-loginpw").val();

       refF.authWithPassword({
        email  : email,
        password : pw
      }, function(error, authData) {
        if (error) {
          console.log("Login Failed!", error);
          $( ".error").removeClass('hide');
        } else {
           location.href="devices.html";
        }
      });
    });

    $( "#btn-reset").click(function() {
        var reEmail = $( "#top-input-pwreset").val();
        refF.resetPassword({
          email : reEmail
        }, function(error) {
          if (error === null) {
            console.log("Password reset email sent successfully");
            location.reload(
                $(document).ready(function(){
                    $( "div.main").append('<p class="reset-it">Die Email mit einem neuem Passwort wurde versand</p>');
                }));
          } else {
            console.log("Error sending password reset email:", error);
            location.reload(
                $(document).ready(function(){
                    $( ".error").removeClass('hide');
                    console.log('erre');
                }));
          }
        });
     });

    $( "#btn-hide-overview").click(function() {
        $("div.overlay").removeClass('show');
    });   
});